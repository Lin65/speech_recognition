#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 11:45:38 2018

@author: xiaolin
"""
import numpy as np
from keras.callbacks import ModelCheckpoint
from keras.models import Model, load_model, Sequential
from keras.layers import Dense, Activation, Dropout, Input, Masking, TimeDistributed, LSTM, Conv1D,Conv2D
from keras.layers import GRU, Bidirectional, BatchNormalization, Reshape,Flatten,MaxPooling2D,GlobalMaxPooling2D
from keras.optimizers import Adam, SGD
from keras.utils import to_categorical
from keras.layers import Dense, Dropout, Activation, Flatten, Input



class Alexnet(object):
    def __init__(self, dropratio = 0.4, dense_units):
        self.dropratio = dropratio
        self.dense_units = dense_units        
    
    def model(self, input_shape):
        X_input = Input(shape = input_shape)

        X = Conv2D(96, kernel_size = 11, strides=4, activation='relu',padding='same')(X_input)
        X = MaxPooling2D(pool_size=(3,3),strides = 2)(X)
        X = BatchNormalization(axis=-1)(X)

        X = Conv2D(256, kernel_size = 5, padding='same', activation='relu')(X)
        X = MaxPooling2D(pool_size=(3,3),strides = 2)(X)
        X = BatchNormalization(axis=-1)(X)

        X = Conv2D(384, kernel_size = 3, padding='same', activation ='relu')(X)

        X = Conv2D(384, kernel_size = 3, padding='same', activation ='relu')(X)

        X = Conv2D(256, kernel_size = 3, padding='same', activation = 'relu')(X)
        X = MaxPooling2D(pool_size=(3,3),strides = 2)(X)

        X = Flatten()(X)
        X = Dropout(self.dropratio)(X)
        X = Dense(self.dense_units, activation='relu')(X)
        X = Dropout(self.dropratio)(X)
        X = Dense(self.dense_units, activation='relu')(X)
        X = Dense(12, activation='softmax')(X)

        alexnet = Model(inputs = X_input, outputs = X)
        return alexnet
    
    def build_model(self, input_shape):
        alexnet = self.model(input_shape)
        alexnet.compile(loss='categorical_crossentropy',
             optimizer = SGD(lr=0.001, decay = 1e-6, momentum=0.9),
             metrics=['accuracy'])
        
        alexnet.summary()
        return alexnet
    
    def train_model(self, training_x, training_y, validation_x, validation_y, epochs, batch_size, weights_path):
        input_shape = training_x[0].shape
        alexnet = self.build_model(input_shape)
            
        alexnet.compile(optimizer=SGD(lr=0.01, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])
        # checkpoint
        filepath = weights_path
        checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
        callbacks_list = [checkpoint]

        # training 
        res = alexnet.fit(training_x, training_y,
                    epochs= epochs,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        
        # change learning rate
        alexnet.load_weights(weights_path)
        alexnet.compile(optimizer=SGD(lr=0.0001, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])

        # training 
        res = alexnet.fit(training_x, training_y,
                    epochs= epochs,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        # log
        history = np.stack((res.history['acc'],res.history['val_acc']),axis = 1)
        np.savetxt('alexnet_log.txt',history)

