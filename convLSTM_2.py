#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 14:41:16 2018

@author: xiaolin
"""

from keras.layers import Conv2D, LSTM, GRU, Dense, MaxPooling2D, Dropout, Flatten, Input, Activation
from keras.layers import Bidirectional, Concatenate, Permute, Dot, Multiply, Reshape, Lambda, RepeatVector, BatchNormalization
from keras.utils import to_categorical
from keras.models import Model
from keras.optimizers import Adam, SGD
import keras.backend as K
import numpy as np
import pandas as pd
from keras.callbacks import ModelCheckpoint

class ConvGRU_2(object):
    def model(self, input_shape):
        X_input = Input(shape = (64,64,1))
        X = Conv2D(32, kernel_size = (5,5), strides = (3,1), activation = 'relu')(X_input)

        X = Permute((2,1,3))(X)
        X = Reshape((60, 20*32))(X)

        X = Bidirectional(GRU(units = 256, return_sequences = True))(X)
        X = Bidirectional(GRU(units = 256, return_sequences = False))(X)

        X = Dropout(0.4)(X)
        X = Dense(512, activation='relu')(X)
        X = Dense(12, activation='softmax')(X)

        convGRU = Model(inputs = X_input, outputs = X)
        return convGRU
    
    def build_model(self, input_shape):
        convGRU = self.model(input_shape)
        convGRU.compile(loss='categorical_crossentropy',
             optimizer = SGD(lr=0.001, decay = 1e-6, momentum=0.9),
             metrics=['accuracy'])
        
        convGRU.summary()
        return convGRU
    
    def train_model(self, training_x, training_y, validation_x, validation_y, epochs, batch_size, weights_path):
        input_shape = training_x[0].shape
        convGRU = self.build_model(input_shape)
            
        convGRU.compile(optimizer=SGD(lr=0.01, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])
        # checkpoint
        filepath = weights_path
        checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
        callbacks_list = [checkpoint]

        # training 
        res = convGRU.fit(training_x, training_y,
                    epochs= epochs,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        
        # change learning rate
        convGRU.load_weights(weights_path)
        convGRU.compile(optimizer=SGD(lr=0.0001, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])

        # training 
        res = convGRU.fit(training_x, training_y,
                    epochs= epochs,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        # log
        history = np.stack((res.history['acc'],res.history['val_acc']),axis = 1)
        np.savetxt('convGRU_2_log.txt',history)
    

