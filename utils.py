#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 11 14:39:01 2018

@author: xiaolin
"""
import librosa
import os
import numpy as np

TRAIN_AUDIO_PATH = 'data/train/audio/'
NOISE_AUDIO_PATH = TRAIN_AUDIO_PATH + '_background_noise_/'

AUDIO_SR = 16000
AUDIO_LENGTH = 16000

# generate silence audio clip from background noise audio
def silence_generation(audio_length):
    waves = [f for f in os.listdir(NOISE_AUDIO_PATH) if f.endswith('.wav')]
    wave_index = np.random.randint(0,len(waves))
    
    wave = librosa.core.load(NOISE_AUDIO_PATH + waves[wave_index], sr=AUDIO_SR)[0]
    start = np.random.randint(0, len(wave) - audio_length)
    end = start + audio_length
    alpha = np.random.random()
    wave = np.clip(wave[start: end]*alpha,-1,1)
    return wave
    
# add noise to audio clip
def data_add_noise(wave, u=0.7):
    if np.random.random() < u:        
        noise = silence_generation(AUDIO_LENGTH)
        alpha = max(abs(noise))/max(abs(wave))
        if alpha < 0.5:
            wave  = noise + wave
        else:
            wave = 0.5/alpha*noise + wave
    return wave

# pad shorter audio with zeros on both ends 
def data_padding(wave):
    if len(wave)<AUDIO_LENGTH:
        L = abs(len(wave)-AUDIO_LENGTH)
        start = L//2
        wave  = np.pad(wave, (start, L-start), 'constant')
    return wave

# convert audio wave to spectrogram
def data_wave_to_melspectrogram(wave):
    spectrogram = librosa.feature.melspectrogram(wave, sr=AUDIO_SR, n_mels=64, hop_length=251, n_fft=753, fmin=20, fmax=8000)
    spectrogram = librosa.power_to_db(spectrogram)
    spectrogram = spectrogram.astype(np.float32)
    return spectrogram

# time shift
def data_shift(wave, left = 1000, right = 500):
    shift = np.random.randint(-right, left)
    a = -min(0, shift)
    b = max(0, shift)
    data = np.pad(wave, (a, b), 'constant')
    return data[:len(data)-a] if a else data[b:]

# scale
def data_scale(wave):
    scale = [0.8, 0.9, 1.0, 1.1, 1.2]
    alpha = scale[np.random.randint(0,5)]
    wave = alpha*wave
    return wave
    

