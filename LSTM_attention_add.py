#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 15:12:07 2018

@author: xiaolin
"""

from keras.layers import Bidirectional, Concatenate, Permute, Dot, Input, LSTM, Multiply,Reshape,Add
from keras.layers import RepeatVector, Dense, Activation, Lambda,Dropout,Multiply,Flatten
from keras.optimizers import Adam,SGD
from keras.utils import to_categorical
from keras.models import load_model, Model
import keras.backend as K
import numpy as np
from keras.callbacks import ModelCheckpoint

class LSTM_attention_add(object):
    def one_step_attention(self, a):
        '''
        a -- hidden state output of the Bi-LSTM, numpy array of shape (m, Tx, 2*n_a)
        s_prev -- previous hidden state of the post-LSTM, shape (m, n_s)
    
        returns:
        context -- vector to the post-LSTM cell
        '''
        last_a = Lambda(lambda x: x[:,-1,:])(a)    
        last_c = RepeatVector(64)(last_a)
    
        cc = Dense(512, use_bias = False)(last_c)
        aa = Dense(512, use_bias = False)(a)
    
        m = Add()([cc, aa])
        #m = Concatenate(axis=-1)([last_c, a])
    
        e = Activation('tanh')(m)    
        energies = Dense(1, activation = 'relu')(e)
    
        alphas = Activation('softmax', name = 'attention_weights')(energies)    
        context =  Dot(axes = 1)([alphas, a])    
        context = Reshape((1024,))(context)    
        context = Concatenate(axis=-1)([context, last_a])    
        return context   

    def model(self, input_shape):    
        X_input = Input(shape = input_shape)  
        X = Bidirectional(LSTM(units = 512, return_sequences = True))(X_input)                                 
        X = Bidirectional(LSTM(units = 512, return_sequences = True))(X)        
        X = self.one_step_attention(X)
        X = Dropout(0.4)(X)
        X = Dense(512, activation = 'tanh')(X)     
        X = Dense(12, activation = 'softmax')(X)

        model = Model(inputs = X_input, outputs = X)    
        return model

    
    def build_model(self, input_shape):
        lstm = self.model(input_shape)
        lstm.compile(loss='categorical_crossentropy',
             optimizer = SGD(lr=0.001, decay = 1e-6, momentum=0.9),
             metrics=['accuracy'])
        
        lstm.summary()
        return lstm
    
    def train_model(self, training_x, training_y, validation_x, validation_y, epochs, batch_size, weights_path):
        input_shape = training_x[0].shape
        lstm = self.build_model(input_shape)
            
        lstm.compile(optimizer=SGD(lr=0.01, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])
        # checkpoint
        filepath = weights_path
        checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
        callbacks_list = [checkpoint]

        # training 
        res = lstm.fit(training_x, training_y,
                    epochs= epochs,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        
        # change learning rate
        lstm.load_weights(weights_path)
        lstm.compile(optimizer=SGD(lr=0.0001, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])

        # training 
        res = lstm.fit(training_x, training_y,
                    epochs= epochs,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        # log
        history = np.stack((res.history['acc'],res.history['val_acc']),axis = 1)
        np.savetxt('lstm_attention_add_log.txt',history)
