#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 11:58:11 2018

@author: xiaolin
"""

from keras.callbacks import ModelCheckpoint
from keras.models import Model, load_model, Sequential
from keras.layers import Dense, Activation, Dropout, Input, Masking, TimeDistributed, LSTM, Conv1D,Conv2D
from keras.layers import GRU, Bidirectional, BatchNormalization, Reshape,Flatten
from keras.optimizers import Adam, SGD
from keras.utils import to_categorical
import numpy as np
import pandas as pd

from keras.applications.vgg16 import VGG16


class VGG(object):
    def __init__(self, dropratio = 0.4, dense_units):
        self.dropratio = dropratio
        self.dense_units = dense_units
        
    def model(self, input_shape):
        pre_vgg16 = VGG16(weights='imagenet', include_top=False)

        X_input = Input(shape = input_shape)
        X = pre_vgg16(X_input)
        X = Flatten()(X)
        X = Dropout(self.dropratio)(X)
        X = Dense(self.dense_units, activation='relu')(X)
        X = Dropout(self.dropratio)(X)
        X = Dense(self.dense_units, activation='relu')(X)
        X = Dense(12, activation='softmax')(X)

        vgg16 = Model(inputs = X_input, outputs = X)        
        return vgg16
    
    def build_model(self, input_shape):
        vgg16 = self.model(input_shape)
        vgg16.compile(loss='categorical_crossentropy',
             optimizer = SGD(lr=0.001, decay = 1e-6, momentum=0.9),
             metrics=['accuracy'])
        
        vgg16.summary()
        return vgg16
    
    def train_model(self, training_x, training_y, validation_x, validation_y, epochs1, epochs2, batch_size, weights_path):
        input_shape = training_x[0].shape
        vgg16 = self.build_model(input_shape)
        
        # only train top layers
        for layer in vgg16.layers[1].layers:
            layer.trainable = False

        vgg16.compile(loss='categorical_crossentropy',
             optimizer = SGD(lr=0.001, decay = 1e-6, momentum=0.9),
             metrics=['accuracy'])
        
        vgg16.fit(training_x, training_y,
                  epochs= epochs1,
                  batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True)
        
        
        # fine tune all the layers
        for layer in vgg16.layers[1].layers:
            layer.trainable = True
            
        vgg16.compile(optimizer=SGD(lr=0.0001, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])
        # checkpoint
        filepath = weights_path
        checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
        callbacks_list = [checkpoint]

        # training 
        res = vgg16.fit(training_x, training_y,
                    epochs= epochs2,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        # log
        history = np.stack((res.history['acc'],res.history['val_acc']),axis = 1)
        np.savetxt('vgg_16_log.txt',history)

        

        
        
   

