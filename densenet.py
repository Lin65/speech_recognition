#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 11:54:43 2018

@author: xiaolin
"""

from keras.callbacks import ModelCheckpoint
from keras.models import Model, load_model, Sequential
from keras.layers import Dense, Activation, Dropout, Input, Masking, TimeDistributed, LSTM, Conv1D,Conv2D
from keras.layers import GRU, Bidirectional, BatchNormalization, Reshape,Flatten
from keras.optimizers import Adam, SGD
from keras.utils import to_categorical
import numpy as np
import pandas as pd

from keras.applications.densenet import DenseNet121

class Densenet(object):       
    
    def model(input_shape):    
        pre_DenseNet121 = DenseNet121(weights='imagenet',pooling='avg', include_top=False)

        X_input = Input(shape = input_shape)
        X = pre_DenseNet121(X_input)
        X = Dense(12, activation='softmax')(X)

        denseNet121 = Model(inputs = X_input, outputs = X)    
        return denseNet121
    
    def build_model(self, input_shape):
        densenet = self.model(input_shape)
        densenet.compile(loss='categorical_crossentropy',
             optimizer = SGD(lr=0.001, decay = 1e-6, momentum=0.9),
             metrics=['accuracy'])
        
        densenet.summary()
        return densenet
    
    def train_model(self, training_x, training_y, validation_x, validation_y, epochs, batch_size, weights_path):
        input_shape = training_x[0].shape
        densenet = self.build_model(input_shape)
            
        densenet.compile(optimizer=SGD(lr=0.001, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])
        # checkpoint
        filepath = weights_path
        checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
        callbacks_list = [checkpoint]

        # training 
        res = densenet.fit(training_x, training_y,
                    epochs= epochs,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        
        # change learning rate
        densenet.load_weights(weights_path)
        densenet.compile(optimizer=SGD(lr=0.0001, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])

        # training 
        res = densenet.fit(training_x, training_y,
                    epochs= epochs,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        # log
        history = np.stack((res.history['acc'],res.history['val_acc']),axis = 1)
        np.savetxt('densenet_log.txt',history)



