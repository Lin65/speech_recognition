# Speech_recognition

This repository contains code for speech recognition on Speech Commands Datasets released by TensorFlow. The dataset includes 65,000 one-second long utterances of 30 short words. 

In this project, there are only 12 possible labels for the Test set: 'yes', 'no', 'up', 'down', 'left', 'right', 'on', 'off', 'stop', 'go', 'silence', 'unknown'. Silence dataset need to be generated from background audio. All words except the listed ten words will be classified as 'unknown'

# Dependencies

This software is dependent on the following packages: numpy, scipy, sklearn, keras, pandas, librosa

The ipython_notebook folder contains file running on Amazon AWS EC2 p2.xlarge Instance

# Results

### RNN based models:

GRU: val 92.902%, test 84.717%

LSTM: val 92.955%, test 84.341%

Bidirectional LSTM with attention (additive): val 91.992%, test 82.896%

Bidirectional LSTM with attention (multiplicative): val 92.331%, test 84.047%

variational LSTM with MC dropout: val 93.116%, test 85.968%

### CNN based models:

Alexnet: val 93.793%, test 85.950%

VGG16: val 94.257%, test 86.232%

Densenet121: val 93.900%, test 86.961%

### CNN + RNN

conv+GRU (1 GRU layer): val 94.614% test 87.243%

conv+GRU (1 conv layer): val 94.079% test 86.397%

Ensemble: val 95.256%, test 88.934%
