#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 15:12:02 2018

@author: xiaolin
"""

from keras.callbacks import ModelCheckpoint
from keras.models import Model, load_model, Sequential
from keras.layers import Dense, Activation, Dropout, Input, Masking, TimeDistributed, LSTM, Conv1D
from keras.layers import GRU, Bidirectional, BatchNormalization, Reshape
from keras.optimizers import Adam, SGD
from keras.utils import to_categorical
import numpy as np
import pandas as pd

class GRU(object):
    def __init__(self, dropratio = 0.4, gru_units):
        self.dropratio = dropratio
        self.gru_units = gru_units        
    
    def model(self, input_shape):
    
        X_input = Input(shape = input_shape)
        X = GRU(units = self.gru_units, return_sequences = True)(X_input)    # GRU (use 128 units and return the sequences)
        X = GRU(units = self.gru_units, return_sequences = False)(X)   # GRU (use 128 units and return the sequences)
        
        X = Dense(12, activation = 'softmax')(X)

        GRU = Model(inputs = X_input, outputs = X)    
        return GRU
    
    def build_model(self, input_shape):
        GRU = self.model(input_shape)
        GRU.compile(loss='categorical_crossentropy',
             optimizer = SGD(lr=0.001, decay = 1e-6, momentum=0.9),
             metrics=['accuracy'])
        
        GRU.summary()
        return GRU
    
    def train_model(self, training_x, training_y, validation_x, validation_y, epochs, batch_size, weights_path):
        input_shape = training_x[0].shape
        GRU = self.build_model(input_shape)
            
        GRU.compile(optimizer=SGD(lr=0.01, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])
        # checkpoint
        filepath = weights_path
        checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
        callbacks_list = [checkpoint]

        # training 
        res = GRU.fit(training_x, training_y,
                    epochs= epochs,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        
        # change learning rate
        GRU.load_weights(weights_path)
        GRU.compile(optimizer=SGD(lr=0.0001, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])

        # training 
        res = GRU.fit(training_x, training_y,
                    epochs= epochs,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        # log
        history = np.stack((res.history['acc'],res.history['val_acc']),axis = 1)
        np.savetxt('GRU_log.txt',history)
