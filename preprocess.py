#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 11:34:20 2018

@author: xiaolin
"""

import numpy as np
from keras.utils import to_categorical


#training_x = np.load('training_x.npy')
#training_y = np.load('training_y.npy')
#validation_x = np.load('validation_x.npy')
#validation_y = np.load('validation_y.npy')

AUDIO_LABELS = ['silence', 'unknown', 'yes', 'no', 'up', 'down', 'left', 'right', 'on', 'off', 'stop', 'go']


def preprocess_rnn(AUDIO_LABELS, training_x, training_y, validation_x, validation_y):
    
    label2cat = dict(zip(AUDIO_LABELS, range(12)))
    training_y = [label2cat[y] for y in training_y]
    training_y = to_categorical(training_y, num_classes = 12)

    validation_y = [label2cat[y] for y in validation_y]
    validation_y = to_categorical(validation_y, num_classes = 12)

    mean_x = np.mean(training_x)
    std_x = np.std(training_x)

    training_x = (training_x - mean_x)/std_x
    validation_x = (validation_x - mean_x)/std_x
    
    return training_x, training_y, validation_x, validation_y, mean_x, std_x

def preprocess_cnn(AUDIO_LABELS, training_x, training_y, validation_x, validation_y, channel = 1):

    training_x = np.array([training_x[i].T for i in xrange(len(training_x))])
    validation_x = np.array([validation_x[i].T for i in xrange(len(validation_x))])

    label2cat = dict(zip(AUDIO_LABELS, range(12)))
    training_y = [label2cat[y] for y in training_y]
    training_y = to_categorical(training_y, num_classes = 12)

    validation_y = [label2cat[y] for y in validation_y]
    validation_y = to_categorical(validation_y, num_classes = 12)

    mean_x = np.mean(training_x)
    std_x = np.std(training_x)
    training_x = (training_x - mean_x)/std_x
    validation_x = (validation_x - mean_x)/std_x

    training_x = training_x.reshape(len(training_x), training_x[0].shape[0],training_x[0].shape[1], channel)
    validation_x = validation_x.reshape(len(validation_x), training_x[0].shape[0],training_x[0].shape[1], channel)
    return training_x, training_y, validation_x, validation_y, mean_x, std_x