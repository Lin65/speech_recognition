#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 11 14:42:34 2018

@author: xiaolin
"""

import os
from os.path import isdir, join
from utils import *
import librosa
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


TRAINING_RATIO = 0.8
UNKNWON_RATIO = 0.5

AUDIO_LABELS = ['silence', 'unknown', 'yes', 'no', 'up', 'down', 'left', 'right',
              'on', 'off', 'stop', 'go']

# split training data into two parts: training and validation
def split2file(dirs, to_dir, training_ratio = 0.8, unknown_ratio = 0.5):
    training_f = open(to_dir + 'training_list.txt', 'w') 
    validation_f = open(to_dir + 'validation_list.txt', 'w') 
    
    for direct in dirs:
        waves = [f for f in os.listdir(join(TRAIN_AUDIO_PATH, direct))
                 if f.endswith('.wav')]
        
        total = len(waves)
        if direct in AUDIO_LABELS:
            split = int(training_ratio*total)  
            for i in xrange(0, split):
                training_f.write(direct+ '/' + waves[i] + '\n')                 
            for i in xrange(split, total):
                validation_f.write(direct+ '/' + waves[i] + '\n')
        else:
            split = int(unknown_ratio*training_ratio*total)
            for i in xrange(0, split):
                training_f.write(direct+ '/' + waves[i] + '\n') 
            for i in xrange(total - int(0.01*total), total):
                validation_f.write(direct+ '/' + waves[i] + '\n')    
            
    training_f.close()
    validation_f.close()
    
# extract trainging data and validation data, save as .npy
class DataExtraction():
    def __init__(self, txtfile, aug = 10, silence_ratio = 0.091, dataset = 'training'):
        self.txtfile = txtfile
        self.labels = []
        self.waves = []
        self.spectrograms = []
        self.silence_ratio = silence_ratio
        self.dataset = dataset
        
        if self.dataset == 'training':
            self.aug = aug
        elif self.dataset == 'validation':
            self.aug = 1
  
        
        f = open(txtfile, 'r')
        
        for line in f:
            line = line.strip()
            
            wave = librosa.core.load(TRAIN_AUDIO_PATH + line, sr=AUDIO_SR)[0]            
            # some audio clips are empty
            if max(abs(wave)) == 0:
                continue
            
            label = line.split('/')[0]            
            
            if label not in AUDIO_LABELS:
                label = 'unknown'
                self.labels.append(label)
            else:
                for _ in xrange(self.aug):
                    self.labels.append(label)
                
            if label == 'unknown':
                self.waves.append(wave.astype(np.float32))
            else:   
                for _ in xrange(self.aug):
                    self.waves.append(wave.astype(np.float32))
            
    def save_data(self, path1, path2, path3):
        np.save(path1, np.array(self.waves))
        np.save(path2, np.array(self.spectrograms))
        np.save(path3, np.array(self.labels))
        
        print 'shape of path1: ',np.array(self.waves).shape
        print 'shape of path2: ',np.array(self.spectrograms).shape
        print 'shape of path3: ',np.array(self.labels).shape
        
    
    def data_augmentation(self):   
        if self.dataset == 'training':
            for i in xrange(len(self.waves)):
                self.waves[i] = data_padding(self.waves[i])
                self.waves[i] = data_shift(self.waves[i])
                self.waves[i] = data_add_noise(self.waves[i])
                self.waves[i] = data_scale(self.waves[i]) 
        elif self.dataset == 'validation':
            for i in xrange(len(self.waves)):
                self.waves[i] = data_padding(self.waves[i])            
            
    def data_transformation(self):
        for i in xrange(len(self.waves)):
            spectrogram = data_wave_to_melspectrogram(self.waves[i])
            self.spectrograms.append(spectrogram.T)
            
    def adding_silence_data(self):
        num_silence = int(len(self.waves)*self.silence_ratio)
        for _ in xrange(num_silence):
            wave = silence_generation(AUDIO_LENGTH)
            self.waves.append(wave.astype(np.float32))
            self.labels.append('silence')



if __name__ == '__main__':
    
    dirs = [f for f in os.listdir(TRAIN_AUDIO_PATH) if isdir(join(TRAIN_AUDIO_PATH,f))]
    dirs.remove('_background_noise_')
    to_dir = 'data/train/'

    split2file(dirs, to_dir)
    
    training_pp = DataExtraction(to_dir + 'training_list.txt', aug=10, dataset ='training')
    training_pp.data_augmentation()
    training_pp.adding_silence_data()
    training_pp.data_transformation()
    training_pp.save_data(to_dir + 'training_x_waves.npy', to_dir + 'training_x.npy', to_dir + 'training_y.npy')
    
    series = pd.Series(training_pp.labels).value_counts()
    
    plt.figure()
    hist = sns.barplot(x = list(series.index), y = series.values, color="salmon")
    hist.set_xticklabels(AUDIO_LABELS, rotation=90)
    hist.set_xlabel('audio')
    hist.set_ylabel('frequency')
    plt.title('training dataset')
    
    
    validation_pp = DataExtraction(to_dir + 'validation_list.txt', dataset = 'validation')
    validation_pp.data_augmentation()
    validation_pp.adding_silence_data()
    validation_pp.data_transformation()
    validation_pp.save_data(to_dir + 'validation_x_waves.npy', to_dir + 'validation_x.npy', to_dir + 'validation_y.npy')
    
    series = pd.Series(validation_pp.labels).value_counts()
    
    plt.figure()
    hist = sns.barplot(x = list(series.index), y = series.values, color="salmon")
    hist.set_xticklabels(AUDIO_LABELS, rotation=90)
    hist.set_xlabel('audio')
    hist.set_ylabel('frequency')  
    plt.title('validation dataset')
    
'''    
### test
import IPython.display as ipd

rand_index = np.random.randint(0, 20000)
print 'audio label: ', training_pp.labels[rand_index]
samples = training_pp.waves[rand_index]
plt.plot(samples)

spectrogram = training_pp.spectrograms[rand_index].T
plt.figure(figsize=(15,4))
sns.heatmap(spectrogram, cmap='viridis')
plt.gca().invert_yaxis()

'''   
    
'''   
sns.heatmap(validation_x[10].reshape(64,64).T, cmap='viridis')    
plt.gca().invert_yaxis()
'''    
    
    
    
    
    