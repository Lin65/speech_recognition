#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 15:12:05 2018

@author: xiaolin
"""

from keras.callbacks import ModelCheckpoint
from keras.models import Model, load_model, Sequential
from keras.layers import Dense, Activation, Dropout, Input, Masking, TimeDistributed, LSTM, Conv1D
from keras.layers import GRU, Bidirectional, BatchNormalization, Reshape
from keras.optimizers import Adam, SGD
from keras.utils import to_categorical
import numpy as np
import pandas as pd

class LSTM(object):
    def __init__(self, dropratio = 0.25, lstm_units):
        self.dropratio = dropratio
        self.lstm_units = lstm_units        
    
    def model(self, input_shape):    
        X_input = Input(shape = input_shape)
        X = Dropout(self.dropratio)(X_input)
        X = LSTM(units = self.lstm_units, return_sequences = True)(X)    # GRU (use 128 units and return the sequences)
        X = Dropout(self.dropratio)(X)                                  # dropout (use 0.8)
        X = LSTM(units = self.lstm_units, return_sequences = False)(X)   # GRU (use 128 units and return the sequences)
         
        X = Dense(12, activation = 'softmax')(X)

        lstm = Model(inputs = X_input, outputs = X)    
        return lstm
    
    def build_model(self, input_shape):
        lstm = self.model(input_shape)
        lstm.compile(loss='categorical_crossentropy',
             optimizer = SGD(lr=0.001, decay = 1e-6, momentum=0.9),
             metrics=['accuracy'])
        
        lstm.summary()
        return lstm
    
    def train_model(self, training_x, training_y, validation_x, validation_y, epochs, batch_size, weights_path):
        input_shape = training_x[0].shape
        lstm = self.build_model(input_shape)
            
        lstm.compile(optimizer=SGD(lr=0.01, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])
        # checkpoint
        filepath = weights_path
        checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
        callbacks_list = [checkpoint]

        # training 
        res = lstm.fit(training_x, training_y,
                    epochs= epochs,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        
        # change learning rate
        lstm.load_weights(weights_path)
        lstm.compile(optimizer=SGD(lr=0.0001, decay = 1e-6, momentum=0.9), loss='categorical_crossentropy', metrics=['accuracy'])

        # training 
        res = lstm.fit(training_x, training_y,
                    epochs= epochs,
                    batch_size= batch_size, validation_data=(validation_x, validation_y), shuffle = True, callbacks=callbacks_list)

        # log
        history = np.stack((res.history['acc'],res.history['val_acc']),axis = 1)
        np.savetxt('lstm_log.txt',history)
